import { MainModel } from "./main.model";

export class Player extends MainModel {
  constructor(public _id: string='', public username: string = "", public profilId: string = "") {
    super(_id);
  }
}
