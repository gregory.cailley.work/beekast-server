export abstract class MainModel {
  constructor(public _id: string='') {}

  withId(id: string) {
    this._id = id;
    return this;
  }
  get id():string {
    return this._id;
  }
}
