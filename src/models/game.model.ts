import { MainModel } from "./main.model";
import { Player } from "./player.model";

export class Game extends MainModel {
  constructor(
    public _id: string = "",
    public timer: number = 2,
    public player1?: Player,
    public player2?: Player,
    public startTime?: number,
    public winner?: Player
  ) {
    super(_id);
  }
  get players() {
    return [this.player1, this.player2];
  }
}
