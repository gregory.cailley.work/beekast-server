import { MainModel } from "../../models/main.model";

/**
 * Class with item model avaiable.
 */
export class DataStorage<T extends MainModel> {
  static delayWrite = 0;
  static delayRead = 0;
  private _items: { [id: string]: T } = {};
  private _nbItems: number = 0;

  async addItem(item: T): Promise<T> {
    return new Promise((resolve) => {
      setTimeout(() => {
        const newItem = !this._items[item.id];

        this._items[item.id] = item ;
        if (newItem) {
          this._nbItems++;
        }
        resolve(item);
      }, DataStorage.delayWrite);
    });
  }

  async delItem(item: T): Promise<void> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (this._items[item.id]) {
          delete this._items[item.id];
          this._nbItems--;
        } else {
          reject(`item not found ${item.id}`);
        }
        resolve();
      }, DataStorage.delayWrite);
    });
  }

  async getItem(itemId: string): Promise<T> {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(this._items[itemId]);
      }, DataStorage.delayRead);
    });
  }

  async getItems(): Promise<{ [id: string]: T }> {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(this._items);
      }, DataStorage.delayRead);
    });
  }

  async getNbItems(): Promise<number> {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(this._nbItems);
      }, DataStorage.delayRead);
    });
  }
}
