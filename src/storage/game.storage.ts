import { Game } from "../models/game.model";
import { DataStorage } from "./technical/data.storage";

export class GameStorage extends DataStorage<Game> {
  /**
   * find user by Game Player.
   * @param playerId
   * @returns Game found.
   */
  async findGameByPlayer(playerId: any) {
    return Object.values(await this.getItems()).find((game) => game.player1?.id == playerId || game.player2?.id == playerId);
  }
}
