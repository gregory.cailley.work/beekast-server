import { Player } from "../models/player.model";
import { DataStorage } from "./technical/data.storage";

export class PlayerStorage extends DataStorage<Player> {

    /**
     * Find player except playerIdToFilter.
     * @param playerToFilter player
     * @returns Player
     */
  async findAllPlayerExcepted(playerToFilter) {
    const playerWaiting = await this.getItems();
    const opponentPlayer =  Object.values(playerWaiting).find((item) => {
      return item.id !== playerToFilter.id
    });
    return opponentPlayer;
  }
}
