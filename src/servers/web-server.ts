import { logger } from "../utils/logger";

export class WebServer {
  private _server;
  constructor() {
    this._server = require("http").createServer(function (request, response) {
      logger("Received request for " + request.url);
      response.end();
    });
  }

  addRoutes() {}

  start() {
    this.addRoutes();
    this._server.listen(3000, function () {
      logger("Server is listening on port 3000");
    });
  }

  get server() {
    return this._server;
  }
}
