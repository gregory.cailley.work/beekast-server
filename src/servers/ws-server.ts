import { GameController } from "../controllers/game.controller";
import { PlayerController } from "../controllers/player.controller";
import { GameService } from "../services/game.service";
import { PlayerService } from "../services/player.service";
import { logger } from "../utils/logger";
import { SocketNotificationService } from "../services/technical/socket-notification.service";

export class WsServer {
  private _io;
  protected gameController: GameController;
  protected playerController: PlayerController;

  constructor(private _server) {
    this._io = this._createSocketIo();

    // TODO dependencies injection using injector in node ???
    const socketNotificationService: SocketNotificationService = new SocketNotificationService(this._io); // if no IOC then singleton pattern could be use.
    const playerService = new PlayerService(); // if no IOC then singleton pattern could be use.
    const gameService = new GameService()
    // 
    this.playerController = new PlayerController(socketNotificationService, playerService);
    this.gameController = new GameController(socketNotificationService, playerService, gameService );
  }

  private _createSocketIo() {
    const options = {
      cors: {
        origin: "http://localhost:4200",
        methods: ["GET", "POST"],
      },
    };
    return require("socket.io")(this._server, options);
  }

  get io() {
    return this._io;
  }
  /**
   * Add WebSockets listerners.
   */
  addListeners() {
    this._io.on("connection", (socket) => {
      logger(`Connection ${socket.id} etablished`);
      // TODO - switch constante
      socket.on("Register",  (data: any) => this.onRegister(socket, data));
      socket.on("Action",  () => this.onPlayerAction(socket));
    });
  }

  /**
   * Method to create a new player and it could start a game or move the player is the waiting list.
   *
   * @param socket socket of the current User.
   * @param data data provided by the user to create the profil.
   */
  async onRegister(socket: any, data: string) {
    try {
      console.debug("onRegister - start");
      const dataJson = this.convertString2Json(data);
      await this.playerController.addPlayerToTheWaitingList(socket, socket.id, dataJson);
      await this.gameController.tryToCreateAGameWithPlayer(socket.id);
      console.debug("onRegister - end");
    } catch (err) {
      console.error(err);
    }
  }

  /**
   * Method to deal with player action.
   *
   * @param socket socket of the current User.
   */
  async onPlayerAction(socket: any) {
    try {
      console.debug("onPlayerAction - start");
      const timeActionPlayer = Date.now(); // get when the action has been collected.
      await this.gameController.manageGameWithPlayerAction(socket, socket.id, timeActionPlayer);
      console.debug("onPlayerAction - end");
    } catch (err) {
      console.error(err);
    }
  }

  /**
   * convert in Json;
   * @param data client message to convert
   * @returns
   */
  protected convertString2Json(data): any {
    return JSON.parse(data);
  }
}
