import { Socket } from "socket.io";
import { GameBuilder } from "../builder/gamer.builder";
import { GameService } from "../services/game.service";
import { PlayerService } from "../services/player.service";
import { SocketNotificationService } from "../services/technical/socket-notification.service";
import { logger } from "../utils/logger";

export class GameController {
  private _gameBuilder: GameBuilder;

  constructor(
    private _socketNotificationService: SocketNotificationService,
    private _playerService: PlayerService,
    private _gameService: GameService
  ) {
    this._gameBuilder = new GameBuilder();
  }

  /**
   * Method to update the game with user action.
   *
   * @param socket socket of the current User.
   * @param playerId id of the socket used by the current user.
   * @param timeActionPlayer timestamp of the action received on the server.
   */
  async manageGameWithPlayerAction(socket: Socket, playerId: string, timeActionPlayer) {
    const game = await this._gameService.updateTheGameWithPlayerAction(playerId, timeActionPlayer);
    if (game && game.winner) {
      logger(`${game.winner.id} is the winner`, game.id);
      game.players.forEach((p) => {
        if (p) {
          const status = p.id === game.winner?.id;
          if (status) {
            logger(`${p.id} win`, game.id);
          } else {
            logger(`${p.id} lose`, game.id);
          }
          this._socketNotificationService.emitEndGameBySocketId(p.id, { status: false, winner: status });
        }
      });
      // delete the game
      this._gameService.removeGame(game);
    } else {
      logger(`${playerId} is not the winner it was too late...`, game ? game.id : "?????");
      this._socketNotificationService.emitEndGameBySocket(socket, { status: false, winner: false });
    }
  }

  /**
   * Method to start a new game if possible.
   * we need another user.
   *
   * @param playerId player Id.
   */
  async tryToCreateAGameWithPlayer(playerId: string) {
    logger(`Trying to create a game with ${playerId}`);
    // search for an opppoent to playerId.
    const players = await this._playerService.getAnotherPlayer(playerId);
    if (players.length == 2) {
      const game = await this._gameService.createNewGame(players);
      // creation socket room
      if (game) {
        this.startANewGame(game);
      }
    } else {
      logger("Not enough player to start a new game");
    }
  }

  /**
   * methode to prepare the game which is about to start.
   * @param game the game to start.
   */
  protected startANewGame(game) {
    logger("Creating new Game", game.id);
    game.players.map((player) => {
      //send game information (the object send has only required information)
      this._socketNotificationService.emitGameConfigurationBySocketId(
        player.id,
        this._gameBuilder.createGameDto(game, player)
      );
      //create a room to emit at all user
      this._socketNotificationService.joinRoom(player.id, game.id);
    });
    //give few second to broswer to be ready before emiting the start.
    setTimeout(() => {
      this.startGame(game);
    }, 500);
  }

  /**
   * method to start a game in a room.
   *
   * @param io io socket object
   * @param game the game to emit the 'start'
   */
  protected startGame(game) {
    logger("Sending signal to start the game", game.id);
    this._socketNotificationService.emitStartGameByRoomId(game.id, { status: true });
  }
}
