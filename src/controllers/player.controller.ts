import { PlayerBuilder } from "../builder/player.builder";
import { Player } from "../models/player.model";
import { PlayerService } from "../services/player.service";
import { SocketNotificationService } from "../services/technical/socket-notification.service";

export class PlayerController {
    private _playerBuilder : PlayerBuilder= new PlayerBuilder();

    constructor(private _socketNotificationService: SocketNotificationService, protected _playerService: PlayerService) {
    }


    /**
   * Method to create a new player and it could start a game or move the player is the waiting list.
   *
   * @param socket socket of the current User.
   * @param playerId id of the socket used by the current user.
   * @param data data provided by the user to create the profil.
   */
  async addPlayerToTheWaitingList(socket: any, playerId, dataJson: any) {
    // Validation
    if (!dataJson.username || !(dataJson.avatarProfil && dataJson.avatarProfil.id) || !playerId) {
      throw new Error(`Player profil is invalide`);
    }
    // action send message with information
    // add player to the player storage.
    const player: Player = this._playerBuilder.createPlayer(playerId, dataJson);

    await this._playerService.addPlayer(player);

    //notify current user of the registration
    this._socketNotificationService.emitPlayerCreatedBySocket(socket, player);
  }
}