/**
 * Modele to send to the clients.
 */
export class GameDto {
  constructor(public timer: number, public otherPlayerUsername: string, public otherPlayerProfilId: string) {}
}
