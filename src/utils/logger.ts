export function logger(message, gameId = ' ') {
  
    console.log(`${process.getuid()} - ${new Date().toLocaleString()} - ${gameId} - ${message}`);
  } 