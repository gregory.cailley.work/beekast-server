import {WebServer} from './servers/web-server';
import {WsServer} from './servers/ws-server';

const webServer = new WebServer();
const wsServer = new WsServer(webServer.server);
wsServer.addListeners();


// START 
webServer.start();
