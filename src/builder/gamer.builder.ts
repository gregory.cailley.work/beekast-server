const randomInt = require("random-int");
import { Game } from "../models/game.model";
import { Player } from "../models/player.model";
import { v4 as uuidv4 } from "uuid";
import { GameDto } from "../dto/game.dto";

export class GameBuilder {
  constructor() {}

  /**
   * Create a new player.
   * 
   * @param id player id
   * @returns Game
   */
  createGame(player1: Player, player2: Player, timerMin:number, timerMax:number): Game {
    return new Game(this.generateId(), this.getRandomTimer(timerMin, timerMax), player1, player2);

  }

  /**
   * Create a DTO object for the client.
   * 
   * @param game game business model
   * @param currentPlayer current player
   * @returns game to send to Client
   */
  createGameDto(game: Game, currentPlayer: Player): GameDto | null {
    const otherPlayer = game.players.find((player) => player?.id != currentPlayer.id);
    if (otherPlayer) {
      return new GameDto(game.timer, otherPlayer.username, otherPlayer.profilId);
    } else {
      return null;
    }
  }

  /**
   * generate number beetween timerMin and timerMax.
   * 
   * @param timerMin timer min.
   * @param timerMax timer max.
   * @returns timer of the game
   */
  getRandomTimer(timerMin:number, timerMax:number) {
    return randomInt(timerMax-timerMin) + timerMin;
  }

  /**
   * Id of the Game
   * 
   * @returns random value
   */
  generateId() {
    return uuidv4();
  }
}
