import { Player } from "../models/player.model";

export class PlayerBuilder {
  constructor() {}

  /**
   * Create a new player.
   * 
   * @param id player id
   * @returns new Player
   */
  createPlayer(id: string, data: { username: string; avatarProfil: { id: string } }): Player {
    return new Player(id, data.username, data.avatarProfil.id);
  }
}
