import { Player } from "../models/player.model";
import { PlayerStorage } from "../storage/player.storage";

/**
 * Business Class for player.
 */
export class PlayerService {
  private _store: PlayerStorage = new PlayerStorage();

  /**
   * WARNING :
   * event can try to call this method at the same time, causing a player in many game at the same time
   *  ==> semaphore, lock needed here .
   *
   * extract another player from the store to play wiht the playerId.
   * both players a remove from the store if there are both found.
   * if not enough player no one is extracted from the store and we retrun [].
   *
   * @param playerId the current player who is searching for another player
   */
  async getAnotherPlayer(playerId: string) {
    const currentPlayer = await this._store.getItem(playerId);
    let opponentPlayers : Player[]= [];
    if (currentPlayer) {
      opponentPlayers = await this._getAnotherPlayer(currentPlayer);
    }
    return opponentPlayers;
  }

  /**
   * extract another player from the store to play wiht the playerId.
   * both players a remove from the store if there are both found.
   * if not enough player no one is extracted from the store and we retrun [].
   *
   * @param currentPlayer the current player
   */
  private async _getAnotherPlayer(currentPlayer: Player) {
    const numberOfPlayerWaiting = await this._store.getNbItems();
    // the current player and another one are needeed
    if (numberOfPlayerWaiting >= 2 && currentPlayer) {
      const newPlayer = await this._store.findAllPlayerExcepted(currentPlayer);
      if (newPlayer) {
        const players = [currentPlayer, newPlayer];
        //remove from player storage
        players.forEach(async (player) => {
          await this._store.delItem(player)
        });
        return [currentPlayer, newPlayer];
      }
    }
    return [];
  }

  async getNbPlayerWaiting() {
    return await this._store.getNbItems();
  }
  async getPlayerById(playerId: string) {
    return await this._store.getItem(playerId);
  }
  async addPlayer(player: Player) {
    return await this._store.addItem(player);
  }
}
