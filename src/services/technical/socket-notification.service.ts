import { Socket } from "socket.io";
import { GameDto } from "../../dto/game.dto";
import { Player } from "../../models/player.model";

export class SocketNotificationService {

  constructor(private _io) {}
  /**
   * Fetch the socket with the ID provied.
   *
   * @param id socketId
   * @returns Socket found
   */
  fetchSocketById(id): Socket {
    return this._io.of("/").sockets.get(id);
  }

  /**
   * Fetch room Socket by ID provided.
   *
   * @param id room ID
   * @returns Room found
   */
  fetchRoomById(id) {
    return this._io.to(id);
  }

  /**
   * emit the end of the Game to a player with the status of the game and if the player is the winner.
   *
   * @param socketId
   * @param data
   */
  emitEndGameBySocketId(socketId: string, data: { status: boolean; winner: boolean }) {
    this.emitEndGameBySocket(this.fetchSocketById(socketId), data);
  }
  emitEndGameBySocket(socket: Socket, data: { status: boolean; winner: boolean }) {
    socket.emit("EndGame", data);
  }

  /**
   * emit GameConfiguration message to clients.
   *
   * @param socketId socketId to notify
   * @param data
   */
  emitGameConfigurationBySocketId(socketId: string, data: GameDto | null) {
    this.fetchSocketById(socketId).emit("GameConfiguration", data);
  }

  /**
   * emit StartGame message to clients.
   * @param roomId
   * @param data
   */
  emitStartGameByRoomId(roomId: string, data: { status: boolean }) {
    this.fetchRoomById(roomId).emit("StartGame", data);
  }

  /**
   * emit PlayerCreated message to client.
   * @param socket socket
   */
  emitPlayerCreatedBySocket(socket: Socket, player:Player) {
    socket.emit("PlayerCreated", `Compte enregistré pour la prochaine partie avec le user ${player.username}`);
  }
  /**
   * Add socket Id to a Room.
   * @param socketId socketId to add to the room.
   * @param roomId
   */
  joinRoom(socketId: string, roomId: string) {
    this.fetchSocketById(socketId).join(roomId);
  }
}
