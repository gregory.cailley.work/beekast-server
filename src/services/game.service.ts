import { GameBuilder } from "../builder/gamer.builder";
import { Game } from "../models/game.model";
import { Player } from "../models/player.model";
import { GameStorage } from "../storage/game.storage";
import { logger } from "../utils/logger";

/**
 * Bussiness Service for Game.
 */
export class GameService {
  

  // is injection possible on node ?
  private _gameBuilder: GameBuilder = new GameBuilder();
  private _store: GameStorage = new GameStorage();

  private _timerMin = 2; // min timer
  private _timerMax = 6; // max timer

  constructor() {

  }

  /**
   * We create a Game with players.
   * 
   * @param players players registred to play
   * @returns Game
   */
  async createNewGame(players: Player[]) {
      const game = this._gameBuilder.createGame(players[0], players[1], this._timerMin, this._timerMax );
      await this._storeANewGame(game);
      return game;
  }

  /**
   * seraching for the game with the playerId.
   * 
   * @param playerId player Id
   * @param timeActionPlayer timestamp of the action received on the server.
   */
  async updateTheGameWithPlayerAction(playerId: string, timeActionPlayer: number) {
    const game = await this._store.findGameByPlayer(playerId);    
    if (game && game.startTime) {
      logger(`${playerId} send an action`, game.id);
      if (game.winner) {
        // we dont need to do anything ...
        return game;
      }
      // seaching for the winner
      if (this._isActionPlayerTooSoon(game.startTime, game.timer, timeActionPlayer)) {
        logger(`${playerId} was too soon `, game.id);
        this._initializeLoser(game, playerId);
      } else {
        logger(`${playerId} was the first `, game.id);
        this._initializeWinner(game, playerId);
      }
      return game;
    } else {
      return undefined; // we don't found the game or it's no startTime
    }
  }

  async removeGame(game: Game) {
    await this._store.delItem(game);
  }
  async getNbGames() {
    return await this._store.getNbItems();
  }
  
  private _initializeLoser(game: Game, playerId: string) {
    const playerWinner = game.players.find((p) => (p && p.id) != playerId);
    game.winner = playerWinner;
  }

  private _initializeWinner(game: Game, playerId: string) {
    const playerWinner = game.players.find((p) => (p && p.id) === playerId);
    game.winner = playerWinner;
  }

  /**
   * Method which calcul if the player has send his action too soon.
   *
   * @param timeStartGame timestamp when the game has started.
   * @param timeTimerGame timer before starting the game.
   * @param timeActionPlayer time when the client has send the action.
   * @returns
   */
  private _isActionPlayerTooSoon(timeStartGame: number, timeTimerGame: number, timeActionPlayer: number) {
    return timeStartGame + timeTimerGame * 1000 > timeActionPlayer;
  }

  /**
   * start the game and store it into the data storage.
   * @param game
   */
   private async _storeANewGame(game: Game) {
    game.startTime = Date.now();
    await this._store.addItem(game);
  }


}
