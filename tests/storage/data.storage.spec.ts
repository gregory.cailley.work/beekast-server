import { expect, assert } from "chai";
import { Player } from "../../src/models/player.model";
import { DataStorage } from "../../src/storage/technical/data.storage";

describe("Data Service CRUD for memory database", function () {
  it("add user", function () {
    const userService = new DataStorage<Player>();
    const newPlayer = new Player('testAdd', 'username', 'profileId')
    
    // pre-check
    expect(userService.nbItems).to.be.equals(0);

    userService.addItem(newPlayer);
    expect(userService.nbItems).to.be.equals(1);
    userService.addItem(newPlayer);
    expect(userService.nbItems).to.be.equals(1);
    expect(userService.getItem(newPlayer.id)).to.be.equals(newPlayer);
  });

  it("delete user", function () {
    const userService = new DataStorage<Player>();
    const newPlayerAdd = new Player('testAdd', 'username', 'profileId')
    const newPlayerNotAdded = new Player('testNotAdded', 'username', 'profileId')
    
    // pre-check
    expect(userService.nbItems).to.be.equals(0);
    userService.addItem(newPlayerAdd);
    userService.delItem(newPlayerNotAdded);
    expect(userService.nbItems).to.be.equals(1);
    userService.delItem(newPlayerAdd);
    expect(userService.nbItems).to.be.equals(0);
    
  });
});

