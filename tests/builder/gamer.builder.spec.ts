import { expect, assert } from "chai";
import { GameBuilder } from "../../src/builder/gamer.builder";
import { GameDto } from "../../src/dto/game.dto";
import { Player } from "../../src/models/player.model";

describe("Game Builder", function () {
  it("createGame - with players", function () {
    const gameBuilder = new GameBuilder();
    const newPlayer1 = new Player("1", "player1", "profileId1");
    const newPlayer2 = new Player("2", "player2", "profileId2");

    const game = gameBuilder.createGame(newPlayer1, newPlayer2, 2, 6);

    expect(game).not.to.be.null;
    expect(game.player1).not.to.be.null;
    expect(game.player1?.id).to.be.equal(newPlayer1.id);
    expect(game.player2).not.to.be.null;
    expect(game.player2?.id).to.be.equal(newPlayer2.id);
    expect(game.timer).to.be.gte(2);
    expect(game.timer).to.be.lte(6);
    expect(game.id).not.to.be.equal("");
  });

  it("createGameDto - with players", function () {
    const gameBuilder = new GameBuilder();
    const newPlayer1 = new Player("1", "player1", "profileId1");
    const newPlayer2 = new Player("2", "player2", "profileId2");
    const game = gameBuilder.createGame(newPlayer1, newPlayer2, 3, 5);
    expect(game).not.to.be.null;

    const gameDto = gameBuilder.createGameDto(game, newPlayer1);

    expect(gameDto).not.to.be.null;
    expect(gameDto?.otherPlayerProfilId).to.be.equal(newPlayer2.profilId);
    expect(gameDto?.otherPlayerUsername).to.be.equal(newPlayer2.username);
    expect(gameDto?.timer).to.be.gte(3);
    expect(gameDto?.timer).to.be.lte(5);
  });

  it("createGameDto - with timer specific", function () {
    const gameBuilder = new GameBuilder();
    const newPlayer1 = new Player("1", "player1", "profileId1");
    const newPlayer2 = new Player("2", "player2", "profileId2");
    const game = gameBuilder.createGame(newPlayer1, newPlayer2, 4, 5);
    expect(game).not.to.be.null;

    const gameDto = gameBuilder.createGameDto(game, newPlayer1);
    expect(gameDto).not.to.be.null;
    expect(gameDto?.timer).to.be.gte(4);
    expect(gameDto?.timer).to.be.lte(5);
  });
});
