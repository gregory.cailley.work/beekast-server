import { expect, assert } from "chai";
import { PlayerService } from "../../src/services/player.service";
import { Player } from "../../src/models/player.model";
import { GameService } from "../../src/services/game.service";

describe("Game Service", function () {
  it("tryToCreateAGame - player provided not existing in storage", function () {
    const playerService = new PlayerService();
    const gameService = new GameService(playerService);

    const newPlayer1 = new Player("1", "player1", "profileId1");
    const newPlayer2 = new Player("2", "player2", "profileId2");

    const game = gameService.tryToCreateAGame(newPlayer1);
    expect(game).to.be.null;
  });

  it("tryToCreateAGame - player provided existing in storage but too fex players", function () {
    const playerService = new PlayerService();
    const gameService = new GameService(playerService);

    const newPlayer1 = new Player("1", "player1", "profileId1");
    const newPlayer2 = new Player("2", "player2", "profileId2");

    playerService.addPlayer(newPlayer1);
    const game = gameService.tryToCreateAGame(newPlayer1);
    expect(game).to.be.null;
  });

  it("tryToCreateAGame - player provided existing in storage with many players", function () {
    const playerService = new PlayerService();
    const gameService = new GameService(playerService);

    const newPlayer1 = new Player("1", "player1", "profileId1");
    const newPlayer2 = new Player("2", "player2", "profileId2");
    const newPlayer3 = new Player("3", "player3", "profileId3");

    playerService.addPlayer(newPlayer1);
    playerService.addPlayer(newPlayer2);
    playerService.addPlayer(newPlayer3);
    expect(gameService.getNbGames()).to.be.equal(0);

    const game = gameService.tryToCreateAGame(newPlayer1);
    if (game) {
      expect(game).not.to.be.null;
      expect(game.players.length).to.be.equal(2);
      expect(gameService.getNbGames()).to.be.equal(1);
    } else {
      assert.fail("game is null");
    }
  });
});
