import { expect } from "chai";
import { PlayerService } from "../../src/services/player.service";
import { Player } from "../../src/models/player.model";

describe("Player Service", function () {
  it("getAnotherPlayer - not enough player for a game", function () {
    const playerService = new PlayerService();
    const newPlayer1 = new Player('1', 'player1', 'profileId1');
    const newPlayer2 = new Player('2', 'player2', 'profileId2');
    
    playerService.addPlayer(newPlayer1);
    expect(playerService.getAnotherPlayer(newPlayer1.id).length).to.be.equals(0);
  });

  it("getAnotherPlayer - enough player but the current player is not registered", function () {
    const playerService = new PlayerService();
    const newPlayer1 = new Player('1', 'player1', 'profileId1');
    const newPlayer2 = new Player('2', 'player2', 'profileId2');
    const newPlayer3 = new Player('3', 'player3', 'profileId3');
    
    playerService.addPlayer(newPlayer2);
    playerService.addPlayer(newPlayer3);
    expect(playerService.getAnotherPlayer(newPlayer1.id).length).to.be.equals(0);
  });

  it("getAnotherPlayer - is enough player for a game", function () {
    const playerService = new PlayerService();
    const newPlayer1 = new Player('1', 'player1', 'profileId1');
    const newPlayer2 = new Player('2', 'player2', 'profileId2');
    const newPlayer3 = new Player('3', 'player3', 'profileId3');
    
    playerService.addPlayer(newPlayer1);
    playerService.addPlayer(newPlayer2);
    const players = playerService.getAnotherPlayer(newPlayer1.id)
    expect(players.length).to.be.equals(2);
    expect(players[0].id).to.be.equals(newPlayer1.id);
    expect(players[1].id).to.be.equals(newPlayer2.id);
    expect(playerService.getPlayerById(newPlayer1.id)).to.be.undefined;
    expect(playerService.getPlayerById(newPlayer2.id)).to.be.undefined;
  });

  
});

